package com.gpmtools.rfidcardemulatordatatranslate

import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class MainActivity : AppCompatActivity(), NfcAdapter.ReaderCallback {


    private var nfcAdapter: NfcAdapter? = null

    private val pass = "servicePass"
    private val user = "serviceUser"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        val buttonCount = findViewById<Button>(R.id.buttonSend)
        buttonCount.setOnClickListener {
          GlobalScope.async { sendPass(user, pass, getTransmitData(), getRid(), getRoadName()) }

        }
    }


    private suspend fun sendPass(user: String, pass: String, tag: String, rid: String, roadName: String)
    {
         PassSender.sendPass(user, pass, tag, rid, roadName)
    }

    override fun onTagDiscovered(tag: Tag?) {
        setCardId(tag!!.id)
        val isoDep = IsoDep.get(tag)
        isoDep.connect()
        val response = isoDep.transceive(Utils.hexStringToByteArray(
            getTransmitData()))
        isoDep.close()
    }

    private fun getTransmitData() : String
    {
        return findViewById<EditText>(R.id.editTextDataToReader).text.toString()
    }

    private fun getRid(): String
    {
        return findViewById<EditText>(R.id.editRid).text.toString()
    }

    private fun getRoadName(): String
    {
        return findViewById<EditText>(R.id.editRoadName).text.toString()
    }

    private fun setCardId(cardId : ByteArray?) {
        runOnUiThread {
            var v: TextView = findViewById(R.id.textViewCardId)
            cardId?.let { v.text = Utils.toHex(it) }
            cardId ?: run { v.text = "Can`t read." }
        }
    }

    public override fun onResume() {
        super.onResume()
        nfcAdapter?.enableReaderMode(this, this,
            NfcAdapter.FLAG_READER_NFC_A or
                    NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK,
            null)
    }
    public override fun onPause() {
        super.onPause()
        nfcAdapter?.disableReaderMode(this)
    }
}
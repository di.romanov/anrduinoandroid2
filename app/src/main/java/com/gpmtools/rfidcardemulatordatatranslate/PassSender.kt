package com.gpmtools.rfidcardemulatordatatranslate

import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


object PassSender {

    fun sendPass(user: String, pass: String, tag: String, rid: String, roadName: String)
    {
        val body = "http://${Settings.serverUrl}/setUserPassTag?user=$user&pass=$pass&tag=$tag&rid=$rid&roadName=$roadName"
        println(body)
        val url: URL
        var urlConnection: HttpURLConnection? = null
        try {
            url = URL(body)
            urlConnection = url.openConnection() as HttpURLConnection
            val `in` = urlConnection.inputStream
            val isw = InputStreamReader(`in`)
            var data: Int = isw.read()
            while (data != -1) {
                val current = data.toChar()
                data = isw.read()
                print(current)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            urlConnection?.disconnect()
        }
    }
}